extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Ресурсный центр"


# URL к API
const URL = "https://t34.tehnokom.su/api/v1.1/"


# Запрос к API
func taskoj_query():
	return JSON.print({ 'query': 'query ($realecoId:Float ) { '+
		'universoTasko (realeco_Id:$realecoId) { edges { node { '+
		' uuid nomo { enhavo } priskribo { enhavo } '+
		' } } } }',
		"variables": {"realecoId":Global.realeco} })
