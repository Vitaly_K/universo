extends KinematicBody

export var Sensitivity_X: float = 0.01
export var Sensitivity_Y: float = 0.01

const ZOOM_MIN = 1
const ZOOM_MAX = 50
const Zoom_Step: float = 1.0
#const MIN_ROT_Y = -1.55 #(89 градусов)
#const MAX_ROT_Y = 0.79 #(45 градусов)

var max_speed =1000.0
var current_speed =0
var acceleration = 0.5
var way_point: Vector3 = Vector3() # Координаты точки, в которую летим
var target_dir: Vector3 = Vector3.ZERO #направление на эту точку от текущей позиции корабля
var target_rot #положение корабля, которое надо принять, чтобы нацелиться на точку.
var speed_rotation =0.03
var middle_mouse_pressed = false
var docking_rotation
var uuid #uuid активного корабля игрока
var projekto_uuid

func _ready():
	pass

func _physics_process(delta):
	if target_dir != Vector3.ZERO: #Если цель существует, двигаемся
		target_dir = (way_point - translation).normalized()
		if translation.distance_to(way_point) > max_speed*delta/acceleration:
			current_speed = lerp(current_speed,max_speed,delta*acceleration)
			transform.basis = Basis(Quat(transform.basis).slerp(target_rot,speed_rotation)) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
		else:
			if translation.distance_to(way_point) <0.01:
				if docking_rotation !=null:
					transform.basis = Basis(Quat(docking_rotation))
				translation = way_point
				clear_way_point()
				print("doexali")
				# останавливаем таймер передачи данных на сервер
				$"../timer".stop()
				#отправка последних координат и закрытие задачи с проектом
				finofara_flugo()
				return
			current_speed = lerp(current_speed,50,delta*acceleration)

			if docking_rotation != null:
				transform.basis = Basis(Quat(transform.basis).slerp(docking_rotation,speed_rotation*1.5)) #поворачиваем в дефолтное состояние, чтобы сесть
		move_and_slide(target_dir*delta*current_speed) #Двигаемся к цели
		Title.get_node("CanvasLayer/UI/L_VBox/Objektoj/Window").distance_to(translation)
		$"../b_itinero/itinero".distance_to(translation)


func set_way_point(position, dock):
	docking_rotation = dock
	way_point = position # устанавливаем точку цели
	target_dir = (way_point - translation).normalized() # устанавливаем направление движение на цель.
	target_rot = Quat(transform.looking_at(way_point,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.
		
func clear_way_point():
	target_dir = Vector3.ZERO #очищаем цель
	way_point = Vector3.ZERO
	docking_rotation = null
	current_speed = 0




const QueryObject = preload("../skriptoj/queries.gd")

func _on_timer_timeout():
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду
	$"../http_mutate".request(q.URL, Global.backend_headers, true, 2, 
		q.objecto_mutation(uuid, translation.x, 
			translation.y, translation.z,
			rotation.x, 
			rotation.y, rotation.z)
	)

#добавление в список маршрута с предварительным очишением маршрута
func add_itinero():
	$"../b_itinero/itinero".add_itinero('','', 'точка в космосе', 
		way_point.x, way_point.y, 
		way_point.z, translation.distance_to(way_point))


#передача данных на сервер при отправке корабля по первой цели
func vojkomenco():
	if len($"../b_itinero/itinero".itineroj)==0:
		return 404
	var q = QueryObject.new()
	if !projekto_uuid:#если проекта нет, то создаём
		# цель маршрута берём из itineroj
		var count_itineroj=len($"../b_itinero/itinero".itineroj)-1
		$"../http_projekto".request(q.URL, Global.backend_headers, true, 2,
			q.instalo_projekto(uuid,
				translation.x, #kom_koordX
				translation.y, #kom_koordY
				translation.z, #kom_koordZ
				$"../b_itinero/itinero".itineroj[count_itineroj]['koordinatoX'], #fin_koordX
				$"../b_itinero/itinero".itineroj[count_itineroj]['koordinatoY'], #fin_koordY
				$"../b_itinero/itinero".itineroj[count_itineroj]['koordinatoZ'] #fin_koordZ
		))
	else:#проект есть, изменяем задачу
		# ставим задачу в выполененную
		# надо использовать другой request, не от поседанто, т.к. поседанто используется при /
		#   установке владельца задачи и этот может не успеть отработать
		$"../http_finado".request(q.URL, Global.backend_headers, true, 2, 
			q.finado_tasko($"../b_itinero/itinero".itineroj[0]['uuid_tasko']))
		$"../b_itinero/itinero".itineroj.remove(0)#удаляем задачу
		# изменяем цель проекта
		$"../http_tasko".request(q.URL, Global.backend_headers, true, 2, 
			q.instalo_tasko_koord(
			uuid, projekto_uuid, 
			translation.x, #kom_koordX
			translation.y, #kom_koordY
			translation.z, #kom_koordZ
			$"../b_itinero/itinero".itineroj[0]['koordinatoX'], #fin_koordX
			$"../b_itinero/itinero".itineroj[0]['koordinatoY'], #fin_koordY
			$"../b_itinero/itinero".itineroj[0]['koordinatoZ'] #fin_koordZ
		))
		$"../b_itinero/itinero".FillItemList()
	$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero".disabled=true
	$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next".disabled=false
	$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin".disabled=false
	$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear".disabled=false


func finofara_flugo():
	if len($"../b_itinero/itinero".itineroj)==0:
		return 404 # маршрута нет, закрывать нечего, выходим из процедуры
	if projekto_uuid:
		var q = QueryObject.new()
		# если есть очередь в задачах, то закрываем только текущую задачу и запускаем в работу следующую
		if len($"../b_itinero/itinero".itineroj)==1:
			$"../http_finado".request(q.URL, Global.backend_headers, true, 2, 
				q.finado_projeko_tasko(projekto_uuid, $"../b_itinero/itinero".itineroj[0]['uuid_tasko']))
			$"../b_itinero/itinero".itineroj.clear()
			$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/ItemList".clear()
			projekto_uuid=''
			$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/kom_itinero".disabled=false
			$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_next".disabled=true
			$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_fin".disabled=true
			$"../b_itinero/itinero/canvas/MarginContainer/VBoxContainer/HBoxContainer/itinero_clear".disabled=true
		else:
			$"../http_posedanto".request(q.URL, Global.backend_headers, true, 2, 
				q.finado_tasko($"../b_itinero/itinero".itineroj[0]['uuid_tasko']))
			# берём следующую задачу в работу
			$"../b_itinero/itinero".itineroj.remove(0)
			$"../b_itinero/itinero".FillItemList()
			# изменяем следующую задачу  на "в работе"
			$"../http_finado".request(q.URL, Global.backend_headers, true, 2, 
				q.finado_tasko($"../b_itinero/itinero".itineroj[0]['uuid_tasko'],2))
			var position = Vector3($"../b_itinero/itinero".itineroj[0]['koordinatoX'],
				$"../b_itinero/itinero".itineroj[0]['koordinatoY'],
				$"../b_itinero/itinero".itineroj[0]['koordinatoZ'])
			set_way_point(position,null)
			$"../way_point".set_way_point(position)
			#запускаем таймер
			$"../timer".start()
