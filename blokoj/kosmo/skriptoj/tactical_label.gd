extends Spatial
var cam #камера
var offset #смещение контролов

signal new_way_point(position)

func _ready():
	cam = get_tree().get_root().get_camera() #получаем камеру
	offset =Vector2($TextureRect.rect_size/2) #получаем смещение
	
func _physics_process(delta):
	$TextureRect.rect_position = cam.unproject_position(get_global_transform().origin) -offset

func _on_VisibilityNotifier_camera_entered(camera): #И, как и в случае с вейпойнтом, проверяем, видим объект или нет.
	$TextureRect.visible = true

func _on_VisibilityNotifier_camera_exited(camera):
	$TextureRect.visible = false

func _on_TextureRect_gui_input(event):
	if event.is_action_pressed("left_click") and event.doubleclick:
		emit_signal("new_way_point",get_global_transform().origin)
