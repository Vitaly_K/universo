extends Control

const QueryObject = preload("queries.gd")



#var ItemListContent = []
#			'nomo':
#			'priskribo':
#			'uuid':
#			'koordinatoX':
#			'koordinatoY':
#			'koordinatoZ':
#			distance
#			posedantoId
#_to = $"../ship".translation.distance_to(Vector3(koordX,koordY,koordZ))


func _on_Close_button_pressed():
	$"canvas/MarginContainer".set_visible(false)


func _resize(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$"canvas/MarginContainer".rect_size += event.relative


func _drag(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$"canvas/MarginContainer".rect_position += event.relative
	

func FillItemList():
	# Заполняет список найдеными объектами
#	for Item in ItemListContent:
##		$"canvas/MarginContainer/VBoxContainer/scroll/ItemList".add_item('('+String(Item['distance'])+')', null, true)
#		get_node("canvas/MarginContainer/VBoxContainer/scroll/ItemList").add_item(
#			'('+String(int(Item['distance']))+') '+Item['nomo'], null, true)
	for item in Global.objektoj:
#		$"canvas/MarginContainer/VBoxContainer/scroll/ItemList".add_item('('+String(Item['distance'])+')', null, true)
		get_node("canvas/MarginContainer/VBoxContainer/scroll/ItemList").add_item(
			'('+String(int(item['distance']))+') '+item['nomo']['enhavo'], null, true)


func distance_to(trans):
#	for obj in ItemListContent:
	for obj in Global.objektoj:
		obj['distance'] = trans.distance_to(Vector3(obj['koordinatoX'],
			obj['koordinatoY'],obj['koordinatoZ']))
	$'canvas/MarginContainer/VBoxContainer/scroll/ItemList'.clear()
	FillItemList()


# Вызывается перед созданием окна
func _on_Objekto_draw():
	var q = QueryObject.new()

	# Делаем запрос к бэкэнду
	$HTTPObjectoRequestFind.request(q.URL, Global.backend_headers, true, 2, q.objecto_query( 2, 3, 2, 1))


func _on_reload_pressed():
	_on_Objekto_draw()
	pass # Replace with function body.


func _on_check_pressed():
	_on_Objekto_draw()
	pass # Replace with function body.


