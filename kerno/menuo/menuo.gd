extends Node


var load_scene : String


func _on_Profilo_pressed():
	get_tree().change_scene('res://blokoj/profilo/profilo.tscn')


func _on_Resurso_Center_pressed():
	$CanvasLayer/UI/L_VBox/RCentro/Window.popup_centered()


func _on_Objektoj_pressed():
	$CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer.set_visible(true)
	
	
func _on_Servilo_pressed():
	get_tree().change_scene('res://blokoj/servilo/servilo.tscn')


func set_visible(visible: bool):
	$CanvasLayer/UI.visible = visible
	

func _on_Taskoj_pressed():
	$CanvasLayer/UI/L_VBox/Taskoj/Window/canvas/MarginContainer.set_visible(true)

func CloseWindow():
	$CanvasLayer/UI/L_VBox/Taskoj/Window/canvas/MarginContainer.set_visible(false)
	$CanvasLayer/UI/L_VBox/Objektoj/Window/canvas/MarginContainer.set_visible(false)


func reloadWindow():
	$CanvasLayer/UI/L_VBox/Taskoj/Window._on_Taskoj_draw()
	$CanvasLayer/UI/L_VBox/Objektoj/Window._on_Objekto_draw()

func _on_cap_pressed():
	$CanvasLayer/UI/Lbar.color = Color(0, 0.407843, 0.407843, 0.784314)
	$CanvasLayer/UI/TButtons/real/realRect.set_visible(true)
	$CanvasLayer/UI/TButtons/cap/capRect.set_visible(false)
	$CanvasLayer/UI/TButtons/com/comRect.set_visible(true)
	$CanvasLayer/UI/TButtons/real/realLabel.set_visible(false)
	$CanvasLayer/UI/TButtons/com/comLabel.set_visible(false)
	$CanvasLayer/UI/TButtons/cap/capLabel.set_visible(true)
	$CanvasLayer/UI/TButtons/real/realRect.color = Color(0, 0.407843, 0.407843, 0.784314)
	$CanvasLayer/UI/TButtons/com/comRect.color = Color(0, 0.407843, 0.407843, 0.784314)
	$CanvasLayer/UI/romb.color = Color(0, 0.407843, 0.407843, 0.784314)

	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=3:
			Global.realeco = 3
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			# если объекта не будет в космосе, то загружать станцию
			if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
				get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
			else:
				get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func _on_com_pressed():
	$CanvasLayer/UI/Lbar.color = Color(0.780392, 0.152941, 0.05098, 0.784314)
	$CanvasLayer/UI/TButtons/real/realRect.set_visible(true)
	$CanvasLayer/UI/TButtons/cap/capRect.set_visible(true)
	$CanvasLayer/UI/TButtons/com/comRect.set_visible(false)
	$CanvasLayer/UI/TButtons/real/realLabel.set_visible(false)
	$CanvasLayer/UI/TButtons/com/comLabel.set_visible(true)
	$CanvasLayer/UI/TButtons/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/TButtons/real/realRect.color = Color(0.780392, 0.152941, 0.05098, 0.784314)
	$CanvasLayer/UI/TButtons/cap/capRect.color = Color(0.780392, 0.152941, 0.05098, 0.784314)
	$CanvasLayer/UI/romb.color = Color(0.780392, 0.152941, 0.05098, 0.784314)

	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=2:
			Global.realeco = 2
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			# если объекта не будет в космосе, то загружать станцию
			if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
				get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
			else:
				get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
#				get_tree().change_scene("res://blokoj/kosmostacioj/ComKosmostacio.tscn")
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func _on_real_pressed():
	$CanvasLayer/UI/Lbar.color = Color(0.34902, 0.533333, 0.878431, 0.784314)
	$CanvasLayer/UI/TButtons/real/realRect.set_visible(false)
	$CanvasLayer/UI/TButtons/cap/capRect.set_visible(true)
	$CanvasLayer/UI/TButtons/com/comRect.set_visible(true)
	$CanvasLayer/UI/TButtons/real/realLabel.set_visible(true)
	$CanvasLayer/UI/TButtons/com/comLabel.set_visible(false)
	$CanvasLayer/UI/TButtons/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/TButtons/com/comRect.color = Color(0.34902, 0.533333, 0.878431, 0.784314)
	$CanvasLayer/UI/TButtons/cap/capRect.color = Color(0.34902, 0.533333, 0.878431, 0.784314)
	$CanvasLayer/UI/romb.color = Color(0.34902, 0.533333, 0.878431, 0.784314)

	var reload = false
	if Global.realeco!=1:
		Global.realeco = 1
		reload = true
		# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
		CloseWindow()
	get_tree().change_scene("res://blokoj/kosmostacioj/Kosmostacio.tscn")
	if reload:
		reloadWindow()



const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

func _on_request_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		var i=0
		for objekt in parsed_resp['data']['universoObjekto']['edges']:
#			Global.direktebla_objekto.append(objekt['node'])
			Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]=objekt['node']
#			Global.direktebla_objekto[i]['kosmo'] = false
			Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]['kosmo'] = false
			i+=1
		# теперь загружаем те объекты, которые из представленных находятся в космосе
		var q = QueryObject.new()

		# Разрегистрируем обработчик сигнала request_completed (вызывается
		# по завершении HTTPRequest)
		$request.disconnect('request_completed', self, '_on_request_request_completed')
		# Регистрируем новый обработчик (для обработки ответа на запрос по управляемым объектам)
		$request.connect('request_completed', self, 'get_direktebla_kosmo_request_complete')
		# Делаем запрос к бэкэнду для получения списка управляемых объектов.
		# Ответ будет обрабатываться в функции get_direktebla_request_complete
		var error = $request.request(q.URL_DATA, Global.backend_headers,
			true, 2, q.get_direktebla_kosmo_query())
			
		# Если запрос не выполнен из-за какой-то ошибки
		# TODO: Такие ошибки наверное нужно как-то выводить пользователю?
		if error != OK:
			print('Error in GET (direktebla) Request.')


func get_direktebla_kosmo_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		var uuid = []
		for pars in parsed_resp['data']['filteredUniversoObjekto']['edges']:
			uuid.append(pars['node']['uuid'])
		for objekt in Global.direktebla_objekto:
			if objekt.has('uuid'):
				if objekt['uuid'] in uuid:
					objekt['kosmo']=true
		Global.loading = true
	# Разрегистрируем обработчик сигнала request_completed (вызывается
	# по завершении HTTPRequest)
	$request.disconnect('request_completed', self, 'get_direktebla_kosmo_request_complete')


func komenci_request_complete(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	# Если ответ от бэкэнда содержит данные
#	if parsed_resp.has('data'):

	$request.disconnect('request_completed', self, 'komenci_request_complete')

	if $"/root".get_node_or_null('space'):
		$"/root".get_node('space').emit_signal("load_objektoj")


# ответ на заход в станцию
func _on_eniri_kosmostacio_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	# Если ответ от бэкэнда содержит данные
	if parsed_resp.has('data'):
		pass

	$request.disconnect('request_completed', self, '_on_eniri_kosmostacio_request_completed')
